设置红帽 OpenShift Local
===

[toc]
## 0. 硬件配置
> https://crc.dev/crc/getting_started/getting_started/installing/

|   ID   |   NAME   |   MIN   |
| ---- | ---- | ---- |
|   1  |   CPU   |   4 physical CPU cores   |
|   2  |   MEM   |   10.5 GB of free memory |
|   3  |   DISK   |  35 GB of storage space |

## 1. Download OpenShift Local & Download pull secret

<img src='https://ts3.cn.mm.bing.net/th?id=OSAAS.CAEDCF679CE094D1FA12A2EFD5427542&w=72&h=72&c=17&rs=1&o=6&dpr=2.5&pid=5.1' width=38>https://console.redhat.com/openshift/create/local

<img src='https://gitlab.com/opensu/openshift/-/raw/main/png/crc1.png'>

<img src='https://gitlab.com/opensu/openshift/-/raw/main/png/crc2.png'>

## 2. 安装

<kbd>Download OpenShift Local</kbd> 此处下载的应用

## 3. 为您的环境做好运行红帽 OpenShift Local 的准备

```bash
*$ crc setup
INFO Using bundle path /Users/alex/.crc/cache/crc_vfkit_4.15.17_amd64.crcbundle
INFO Checking if running macOS version >= 13.x
INFO Checking if running as non-root
INFO Checking if crc-admin-helper executable is cached
INFO Checking if running on a supported CPU architecture
INFO Checking if crc executable symlink exists
INFO Checking minimum RAM requirements
INFO Check if Podman binary exists in: /Users/alex/.crc/bin/oc
INFO Checking if running emulated on Apple silicon
INFO Checking if vfkit is installed
INFO Checking if CRC bundle is extracted in '$HOME/.crc'
INFO Checking if /Users/alex/.crc/cache/crc_vfkit_4.15.17_amd64.crcbundle exists
INFO Getting bundle for the CRC executable
INFO Downloading bundle: /Users/alex/.crc/cache/crc_vfkit_4.15.17_amd64.crcbundle...
4.73 GiB / 4.73 GiB [----------------------------------------------------] 100.00% 3.80 MiB/s
INFO Uncompressing /Users/alex/.crc/cache/crc_vfkit_4.15.17_amd64.crcbundle
crc.img:  31.00 GiB / 31.00 GiB [----------------------------------------] 100.00%
oc:  118.78 MiB / 118.78 MiB [-------------------------------------------] 100.00%
INFO Checking if old launchd config for tray and/or daemon exists
INFO Checking if crc daemon plist file is present and loaded
INFO Adding crc daemon plist file and loading it
INFO Checking SSH port availability
Your system is correctly setup for using CRC. Use 'crc start' to start the instance
```

## 4. 创建虚拟机并设置集群

> `pull-secret.txt` 这个文件, 是第一步<kbd>Download pull secret</kbd>下载的

```bash
*$ crc start -p pull-secret.txt
INFO Using bundle path /Users/$USER/.crc/cache/crc_vfkit_4.15.17_amd64.crcbundle
INFO Checking if running macOS version >= 13.x
INFO Checking if running as non-root
INFO Checking if crc-admin-helper executable is cached
INFO Checking if running on a supported CPU architecture
INFO Checking if crc executable symlink exists
INFO Checking minimum RAM requirements
INFO Check if Podman binary exists in: /Users/alex/.crc/bin/oc
INFO Checking if running emulated on Apple silicon
INFO Checking if vfkit is installed
INFO Checking if old launchd config for tray and/or daemon exists
INFO Checking if crc daemon plist file is present and loaded
INFO Checking SSH port availability
INFO Loading bundle: crc_vfkit_4.15.17_amd64...
INFO Creating CRC VM for OpenShift 4.15.17...
INFO Generating new SSH key pair...
INFO Generating new password for the kubeadmin user
INFO Starting CRC VM for openshift 4.15.17...
INFO CRC instance is running with IP 127.0.0.1
INFO CRC VM is running
INFO Updating authorized keys...
INFO Configuring shared directories
INFO Check internal and public DNS query...
INFO Check DNS query from host...
INFO Verifying validity of the kubelet certificates...
INFO Starting kubelet service
INFO Waiting for kube-apiserver availability... [takes around 2min]
INFO Adding user's pull secret to the cluster...
INFO Updating SSH key to machine config resource...
INFO Waiting until the user's pull secret is written to the instance disk...
INFO Changing the password for the kubeadmin user
INFO Updating cluster ID...
INFO Updating root CA cert to admin-kubeconfig-client-ca configmap...
INFO Starting openshift instance... [waiting for the cluster to stabilize]
INFO 5 operators are progressing: authentication, console, image-registry, ingress, network
INFO 2 operators are progressing: console, ingress
INFO Operator console is progressing
INFO Operator console is progressing
INFO All operators are available. Ensuring stability...
INFO Operators are stable (2/3)...
INFO Operators are stable (3/3)...
INFO Adding crc-admin and crc-developer contexts to kubeconfig...
Started the OpenShift cluster.

The server is accessible via web console at:
  https://console-openshift-console.apps-crc.testing

Log in as administrator:
  Username: kubeadmin
  Password: PAHIA-VGjYY-xG5He-aMgfy

Log in as user:
  Username: developer
  Password: developer

Use the 'oc' command line interface:
  $ eval $(crc oc-env)
  $ oc login -u developer https://api.crc.testing:6443
```

## 5. 停止集群

```bash
$ crc delete
```

