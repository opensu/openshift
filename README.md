[toc]

# 1. 判断 集群 就绪
**[student@workstation ~]$**
```bash
curl -s https://gitlab.com/opensu/openshift/-/raw/main/wait.sh | bash
```

# 2. 准备练习环境: 280
**[student@workstation ~]$**
## 2.a 全部
```bash
bash -c "$(curl -s https://gitlab.com/opensu/openshift/-/raw/main/LabStart)"
```
## 2.b 单个
```bash
# 下载保存
curl -O https://gitlab.com/opensu/openshift/-/raw/main/LabStart
```
```bash
# 执行
bash LabStart 4
```